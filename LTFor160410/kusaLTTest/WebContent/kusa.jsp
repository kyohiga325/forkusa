<%@page import="org.apache.commons.lang3.RandomUtils"%>
<%@page import="java.io.IOException"%>
<%@page import="com.sun.beans.util.Cache"%>
<%@page import="java.util.Arrays"%>
<%@ page language="java" 
		 contentType="text/html; charset=UTF-8"
		 session="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="resources/css/w3.css">
<link rel="stylesheet" href="resources/css/kusa.css">
<title>kusa LT</title>

<% HttpSession session = request.getSession(false);
   String separator = System.getProperty("line.separator"); 
   String kotoba;
   switch(RandomUtils.nextInt(0, 4)){
   		case 0: kotoba = "今日はどこに連れてってくれるのー？💚";break;
   		case 1: kotoba = "パンケーキ食べたーい💚";break;
   		case 2: kotoba = "女子力高くなりたーい💚";break;
   		case 3: kotoba = "今日は家で DVD 見よっ💚";break;
   		default: kotoba = "はぁ、はぁ💚";break;
   	}		%>

</head>
<body>
	<div class="w3-container w3-theme-l2">
		<div class="w3-xxxlarge" style="margin: 15px 50px">
			草の会の LT !! ～のちめきメモリアル～
		</div>
	</div>

	<div class="row">
		<div class="w3-col s12 m6 l4">
			<img style="margin: 30px;width: 50%;height: auto;" alt="Norway" src="resources/img/nochi.png">
			<div class="arrow_box">
				<div class="w3-xxlarge">
					<% 
					   // セッションがあるかないか
					   if(session != null){
					   		out.print(session.getAttribute("work") + " の " + session.getAttribute("name") + " さん💚、こんにちは!" + "<br />" + kotoba );
					   }else{
					   		if(request.getHeader("Cookie") != null)
					   		out.print("セッション送ってきてるけど、これ知らんな。 <br />お前誰？");
					   		else out.print("セッションないし、お前誰？");
					   }
					%>
				</div>
			</div>
		</div>
		<div class="w3-col s12 m6 l4">
			<div class="w3-card-4" style="margin-top: 50px">
				<header class="w3-container w3-theme-dark">
					<div class="w3-center w3-xlarge">名前と職業を書いてね💛</div>
				</header>
				<form class="w3-container" action="./KuServlet">
					<label class="w3-label w3-text-teal"><b>あなたの名前は？</b></label> 
					<input class="w3-input w3-border w3-light-grey" type="text" name="name"> 
					
					<label class="w3-label w3-text-teal"><b>あなたの職業は？？</b></label> 
					<input class="w3-input w3-border w3-light-grey" type="text" name="work">
					 
					<input class="w3-check" type="checkbox" name="createSession"> 
					<label class="w3-validate w3-large">のっちに覚えてもらう？</label> 
					<input class="w3-btn w3-xlarge w3-white w3-border w3-round-xlarge"
						type="submit" style="margin: 10px" value="送信！！">
				</form>
			</div>
			<div class="w3-card-4" style="margin-top: 50px">
				<header class="w3-container w3-theme-dark">
					<div class="w3-center w3-xlarge">のっちの記憶から消えちゃう？💛</div>
				</header>
				<form class="w3-container" action="./ForgetKuServlet">
					<input class="w3-btn w3-xlarge w3-white w3-border w3-round-xlarge"
						type="submit" style="margin: 10px" value="はい、喜んで消えますぅ！">
				</form>
			</div>
		</div>
		<div class="w3-col s12 m12 l4">
			<div class="w3-container w3-theme-d2 w3-margin-16">
					<div class="w3-xlarge">HTTP Sesson Information</div>
			</div>
			<ul class="w3-ul w3-card-4 w3-margin-16">
				
				<li class="w3-padding-16">
					<span class="w3-xlarge">Session ID</span><br />
					<span class="w3-small" style="word-break : break-all">サーバ側で持つ情報を識別するための一意な情報</span><br />
					<span class="w3-text-theme" style="word-break : break-all">
					<% if(session != null) out.println(session.getId()); %></span>
				</li>
				<li class="w3-padding-16">
					<span class="w3-xlarge">Max Inactive Interval</span><br />
					<span class="w3-small" style="word-break : break-all">サーバ側で捨てるまでの時間(何もしてないとこの時間で消える)</span><br />
					<span class="w3-text-theme"><% if(session != null) out.println(session.getMaxInactiveInterval()); %></span>
				</li>
				
			</ul>
			<div class="w3-container w3-theme-d2 w3-margin-16">
					<div class="w3-xlarge">HTTP Request Information</div>
			</div>
			<ul class="w3-ul w3-card-4 w3-margin-16">
				<li class="w3-padding-16">
					<span class="w3-xlarge">Cookie</span><br />
					<span class="w3-text-theme" style="word-break : break-all"><%
						StringBuilder cookieValues = new StringBuilder();
						try{ 
							if(request.getHeader("Cookie") != null) out.print(request.getHeader("Cookie"));
							out.print(cookieValues);
						}catch(Exception ioe){}
							%></span>
				</li>
				<%-- <li class="w3-padding-16">
					<span class="w3-xlarge">Session ID</span><br />
					<span><% if(session != null) session.getId(); %></span>
				</li>
				<li class="w3-padding-16">
					<span class="w3-xlarge">Session ID</span><br />
					<span><% if(session != null) session.getId(); %></span>
				</li>
				<li class="w3-padding-16">
					<span class="w3-xlarge">Session ID</span><br />
					<span><% if(session != null) session.getId(); %></span>
				</li> --%>
			</ul>		
		</div>
	</div>



</body>
</html>