package jp.co.kusanokai.test.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/ForgetKuServlet")
public class ForgetKuServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public ForgetKuServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// セッション(Cookie の JSESSIONID)が無かったらセッションを作成しない
		HttpSession session = request.getSession(false);
		if(session != null) session.invalidate();		
		String topPage = "./kusa.jsp";
		
		response.sendRedirect(topPage);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		doGet(request, response);
	}

}
