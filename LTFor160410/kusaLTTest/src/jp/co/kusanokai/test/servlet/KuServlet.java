package jp.co.kusanokai.test.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/KuServlet")
public class KuServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	private final String DEFAULTNAME="恭平";
	private final String DEFAULTWORK="デキる男";
	
    public KuServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		
		
		if(request.getParameter("createSession") != null){
			// セッション(Cookie の JSESSIONID)がくても、セッションを作成する
			HttpSession session = request.getSession(true);
			if(session != null){
				
				if(request.getParameter("name").isEmpty()) session.setAttribute("name", DEFAULTNAME);
				else session.setAttribute("name", request.getParameter("name"));
				if(request.getParameter("work").isEmpty()) session.setAttribute("work", DEFAULTWORK);
				else session.setAttribute("work", request.getParameter("work"));
			}
		}
		
		String topPage = "./kusa.jsp";
		
		response.sendRedirect(topPage);
		// request.getRequestDispatcher("./kusa.jsp").forward(request, response);
		
/*		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}		*/
		
		PrintWriter out = response.getWriter();
		
		String title ="レスポンス返ったお";
		
		String docType = 
				"<!DOCTYPE html>";
		
		out.println(docType + 
				"<html>\n" + 
				"<head>\n" + 
					"<title>" + title + "</title>\n" + 
				"</head>\n" + 
				"<body>\n" + 
					"<h1> 返ってきました </h1>\n" + 
				"");
	
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		
		
	}

}
